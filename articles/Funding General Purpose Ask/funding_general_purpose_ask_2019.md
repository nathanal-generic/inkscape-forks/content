After 15 years of development, Inkscape 1.0 is nearly here!

Inkscape 1.0 marks the culmination of over four hundred volunteers
combining over twenty thousand code contributions to create a graphics
application used by millions.

Inkscape draws the whole world together.  It is made available
free-of-charge, by the community and for the community, and asks in
return only what its users can afford to contribute. It has fueled the
creativity of students in underfunded schools, talented artists of every
background, and entrepreneurs with only modest means but great
ambitions. Giving to Inkscape not only benefits you, but enhances the
lives of countless others at the same time.

The team is excited to be providing 1.0 and proud of the diverse work
done and the changes it brings.  In particular, the new MacOS experience
is a huge need for users, and will be transformational for Inkscape as a
project.

As the software has grown larger and more powerful over the years, the
learning curves have gotten steeper, and the programming efforts more
and more challenging.  A couple dozen have dedicated thousands of hours
to bring the software to this point.

Instilling quality into Inkscape's 1.0 release requires a devotion of
even more time and energy.  Beyond 1.0, Inkscape's fans want to see the
project scale up so they can tackle larger challenges: Color
Management/CMYK, Multi-page SVG, GPU rendering, and attention to tons of
ideas that have been suggested.  You can invest in the success of the
project through funding, to help ensure the team the resources they need
over the long haul.

Signup for a recurring monthly donation to Inkscape's **10 to 1.0**
funding drive.  Or, Contact Us <contact@inkscape.org> to become an
official Inkscape sponsor.

[Include donation links]

All donations are tax-deductable in the US.
