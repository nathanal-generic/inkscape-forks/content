# Inkscape Vectors > Content > Presentations
## LGM2019, Get most of Path Effects

![Remark Preview](slides_doc/previews/remarked.png "Remark Preview")


This is the presentation for the speak on LGM of Mihaela and Jabier about LPE

## Start

To run just open index.html in a browser. 
If you are offline and have problems with local fonts you can run the `python3 run.py` to create a local server.

## Short Cuts

Press "c" to create a clone of the window, and move to the the proyector screen and F11 to fullscreen
Select the first window and press "p" to toogle presenter mode with notes.

## PRESENTATION SUBMIT
### Title: Get the Most Out of Live Path Effects
Save time and optimize your workflow with these tips on
how to get the most out of working with Live Path Effects (LPE) in
Inkscape. Learn the basics during this global overview, including a few
tricks to solve common problems. Change elements in a non-destructive
way and apply them optionally in a stack.

## References
Issue on Gitlab abotu presentation submit: https://gitlab.com/inkscape/vectors/content/issues/28

## Acknowledgments

* Hat tip to anyone whose code, words, translations or design skills were used
* Special thanks to @prkos, @moini and @zigzagmlt for their continued relentless support

